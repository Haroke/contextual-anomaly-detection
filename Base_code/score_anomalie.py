from sklearn.covariance import EmpiricalCovariance
import time as ti
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import scipy
import pickle
import math

#Signwise 
def sign(res,dim=(400,20,3)):    
    dim_n,dim_t,dim_g=dim
    signe=np.sign(res.reshape(dim_n,dim_t,dim_g))
    return(signe)

# Thresholding of extrem values
def score_seuil(s,train,per_seuil=99,dim=(400,20,3),local=True):
    dim_n,dim_t,dim_g=dim
    if(local):
        p_s = np.percentile(np.abs(s),per_seuil,axis=0)
    else:
        p_s = np.percentile(np.abs(s),per_seuil)

    #Seuillage
    if(len(s.shape)==2):
        s=np.sign(s)*np.minimum(np.abs(s),np.zeros((len(train),dim_g))+p_s)
    else:
        s=np.minimum(s,np.zeros(len(train))+p_s)
    return(s)

# Normalisation by anomaly detection threshold.
# 4 approch to choose threshold : (N-Sigma-local, N-Sigma-global, Percentiles-local, Percentiles globales) 
def score_norm(s,train,per_norm,dim=(400,20,3),type_norm=None):
    dim_n,dim_t,dim_g=dim
    
    train=train.astype(int)
    l=list(set(train))
    ps = np.zeros((len(l),s.shape[-1]))
    
   
    for i in range(len(l)):
        flag=(train==i)
        
        # Fexible anomalie thresold (N-sgima) by dimension | Hypothesis : Anomaly distributed homegeneously by dimension.
        if(type_norm=='Nsigma_local'): 
            ps[i]=s[flag].std(axis=0)*inv_cdf_approx(per_norm)
            
        # Fexible anomalie thresold (N-sgima) global :  homogeneous anomaly distribution by dimension.
        elif(type_norm=='Nsigma_global'):
            ps[i]+=s[flag].std()*inv_cdf_approx(per_norm)
        
        #rigid anomalie thresold (Percentiles) by dimension :  heterogeneos anomaly distrubtion by dimension.
        elif(type_norm=='Percentiles_local'):
            percs=(np.abs(np.percentile(np.abs(s[flag]),per_norm,axis=0)))
        
        else: #rigid anomalie thresold (Percentiles) global : homogeneous anomaly distribution by dimension.
            percs=(np.abs(np.percentile(np.abs(s[flag]),per_norm)))
        ps[i]=percs
    return(ps)

#Cut of extrem variance outside of percentiles [pmin,pmax]
def cut(var,pmin,pmax):
    vpmin=np.percentile(var,pmin,axis=0)
    vpmax=np.percentile(var,pmax,axis=0)
    var=np.minimum(var,vpmax)
    var=np.maximum(var,vpmin)
    return(var)

#Re-calucaltion of confiance bondary for visualisation purpose.
def borne(Y,res,v_mean,v_std,train,ps,pmin_cut=0.5,pmax_cut=99.5,q_var=1,p_old=95,p_new=95,dim=(400,20,3)):
    v_std=cut(v_std,pmin_cut,pmax_cut).reshape((dim[0]*dim[1],dim[2]))
    v_mean=cut(v_mean,pmin_cut,pmax_cut).reshape((dim[0]*dim[1],dim[2]))
    lim=np.ones((dim[0]*dim[1],dim[2]))
    train=train.astype(int)
    l=list(set(train))
    for i in range(len(l)):
        f=[train==i]
        lim[tuple(f)]=ps[i]*(inv_cdf_approx(p_new)/inv_cdf_approx(p_old))
    up=np.abs((res-Y))+v_mean+(lim*np.power(v_std,q_var))
    down=np.abs((res-Y))+v_mean-(lim*np.power(v_std,q_var))
    down[down<0]=0
    return(up,down)

#Convolution of the signal "s" with a filter "filt"
def conv_dif(s,filt):
    dim_g=s.shape[-1]
    l=[]
    #Pour chaque dimension de s
    for i in range(dim_g):
        ss=np.copy(s[:,i])
        c_s = np.convolve(np.abs(ss),filt,mode='same')
        l.append(c_s[:,None])
        
    s = np.sign(s) * np.maximum(np.abs(s)*0.0,np.abs(np.concatenate(l,axis=1)))
    return(s)

#Threesold value based on the x% confident interval for a standards normal law.
def inv_cdf_approx(x,sigma=1):
    return(scipy.special.erfinv(x/100) * np.sqrt(2) * sigma)

#Main function : Score computation.
#res = Residual source
#v_mean = biais source
#v_std = variance source.
#Train = Training set.

# Parameter :
#Per_name: normal data ratio (local) a priori (default = 95%)
#Per_norm_v : Normal data ratio (Global) a priori (default = per_norm)
#Per_threshold = maximum percentage threshold on the anomaly score.
#Pmin_cut = Value for the low cut of variance and bias.
#Pmax_cut = Value for the high cut of the variance and bias. 
#d = Power formatting of the final score.
# Type_norm: True = Normalization constant according to a hypothesis of anomaly distribution homogeneous by dimension.       
#     'Nsigma_local' : Flexible thresold by dimension :  heterogeneos anomaly distrubtion by dimension.
#     'Nsigma_global': Fexible thresold global :  homogeneous anomaly distribution by dimension.
#     'Percentiles_local': rigid thresold by dimension global : heterogeneos anomaly distrubtion by dimension
#     'Percentiles_global' (Defaut): rigid thresold global : heterogeneos anomaly distrubtion global

#filt = Convolution filter applied.
# Q parameter q related to the hypothetical distribution of anomalies by context : 
#    q < 1: Anomaly related to the context of high variance.
#    q = 1: Anomaly homogeneously distributed by context
#    q > 1: Anomaly related to the context of low variance.
#Dim = dimension of the series.


def var_give(res,v_mean,v_std,train,per_norm=95,per_seuil=99,pmin_cut=0.5,pmax_cut=95,d=2,
             per_norm_v=None,type_norm=False,filt=[0,1,0],q_var=1,dim=(400,20,3)):
    
    dim_bis=(dim[0]*dim[1],dim[2])
    dim_v=(dim[0],dim[1],1)
    
    if(per_norm_v==None):
        per_norm_v=per_norm
                         
    v_std=v_std.reshape(dim_bis)
    v_mean=v_mean.reshape(dim_bis)
    
    v_std=cut(np.copy(v_std),pmin_cut,pmax_cut)
    v_mean=cut(np.copy(v_mean),pmin_cut,pmax_cut)
    
    #Contextual residue normalisation
    s = ((res-v_mean)/(np.power(v_std,q_var)).reshape(-1,dim[-1]))
    
    #Local score extreme threesolding (to handle extrumum aberation)
    s = score_seuil(conv_dif(s,filt),train,per_seuil,dim,True)

    #Mahalanobis distance to synthesize a global score.
    cov = EmpiricalCovariance().fit(s[train])
    v = cov.mahalanobis(s)
    
    #Global score extreme threesolding (to handle extrumum aberation)
    v = score_seuil(v,train,per_seuil,dim_v,True).reshape((-1,1))
    
    train=train.astype(int)
    l=list(set(train))
    
    #Detection threshold normalisation
    ps = score_norm(s,train,per_norm,dim,type_norm)
    pv = score_norm(v,train,per_norm_v,dim_v,type_norm)
    for i in range(len(l)):
        flag=(train==i)
        s[flag]=s[flag]/ps[i]
        v[flag]=v[flag]/pv[i]
        
    #Take a power of score.
    s = np.sign(s)*np.power(np.abs(s),d)
    v = np.power(v,d)
    return(s,ps,v,pv)

# Estimation of empircal (prior-based) variance estiamtion .
def var_emp(res,flag_day,train,period=20,seas=100,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    res=res.reshape((dim_n,dim_t,dim_g))
    v_std=np.zeros((dim_n,dim_t,dim_g))
    v_mean=np.zeros((dim_n,dim_t,dim_g))
    for i in range(dim_n):
        i_mod=i%seas
        month_arr=(np.arange(dim_n) + period-i_mod)%seas
        f = (np.array(flag_day)==flag_day[i]) & (train[(np.arange(dim_n*dim_t)%dim_t)==1]) & (month_arr<=(2*period))
        # Threeshold of lowest variance estimation.
        v_std[i]=np.maximum(0.1,res[f].std(axis=0))
        v_mean[i]=res[f].mean(axis=0)
    return(v_mean.reshape((dim_n*dim_t,dim_g)),v_std.reshape((dim_n*dim_t,dim_g)))

#Estimation of variance based on a extraction of a RF prediction models through of exploitation the built trees
#Non-optimal implementation 
#Can be done iteratively per tree by storing the information of the leaves beforehand to avoid to compute whole for each element.
def esti_var_rf_aux(c_e):
    import numpy as np
    e=c_e[0]
    n_f=e.shape[1]
    e_train=c_e[1]
    y=c_e[2]
    dim_g=y.shape[-1]
    train_rf=c_e[3]
    var_rf=np.zeros((len(e),dim_g))
    pred_rf=np.zeros((len(e),dim_g))
    #For each element
    for nn,j in enumerate(e):
        #Vector calculating ponderation of learning exmeple for forescasting of the nn th elements 
        cur=np.zeros(len(e_train))    
        #For each tree
        for n,i in enumerate(j):
            #Recovery of elements sharing the same leaf
            v=(e_train[:,n]==i)
            #Recovery of ponderation weight
            cur+=(v/v.sum())
        
        #Flag of elements that contribute to the prediction of nn
        flag_cur=(cur>0)
        
        #Ponderate mean = prediction
        pred_rf[nn]=(1/n_f)*((y[train_rf][flag_cur])*np.tile(cur[flag_cur],(dim_g,1)).T).sum(axis=0)
        #Variance estimation calculated by ponderation.
        var_rf[nn]=np.sqrt((1/n_f)*(np.power((y[train_rf][flag_cur]-pred_rf[nn]),2)*np.tile(cur[flag_cur],(dim_g,1)).T).sum(axis=0))
    return(pred_rf,var_rf)