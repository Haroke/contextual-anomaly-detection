import numpy as np
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

# LSTM E-D factory.
# Input and ouput construction.
def ST_factory(Y,X_lt,dim_f=5,dim=(400,20,3),recon=True,remove=True):
    dim_n,dim_t,dim_g=dim
    Y=Y.reshape(dim_n*dim_t,dim_g)
    dim_lt=X_lt.shape[-1]

    l=[Y]
    for i in range(1,dim_f):
        l.append(np.roll(Y,-i,axis=0))
    Y_all=np.array(l).swapaxes(0,1).reshape(dim_n,dim_t,dim_f,dim_g)

    if(recon):
        l=[Y]
        for i in range(1,dim_f):
            l.append(np.roll(Y,i,axis=0))
        Y_past=np.array(l).swapaxes(0,1).reshape(dim_n,dim_t,dim_f,dim_g)
    else:
        l=[]
        for i in range(1,dim_f+1):
            l.append(np.roll(Y,i,axis=0))
        Y_past=np.array(l).swapaxes(0,1).reshape(dim_n,dim_t,dim_f,dim_g)
        
    if(remove):
        for i in range(dim_f-1):
            Y_past[:,:(i+1),(i+1)] = 0
            Y_all[:,-(i+1):,(i+1)] = 0
    return(Y_all,Y_past)

# Contextual mean
def mean_cat(res,flag_day,train,period=15,seas=100,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    flag_day=np.array(flag_day)
    res=res.reshape((dim_n,dim_t,dim_g))
    mean=np.zeros((dim_n,dim_t,dim_g))
    for i in range(dim_n):
        i_mod=i%seas
        month_arr=(np.arange(dim_n) + period-i_mod)%seas
        f=((train) & (month_arr<=(2*period)) & (flag_day==flag_day[i]))
        mean[i]=res[f].mean(axis=0)
    return(mean)

# Evluation function
def print_res_aux(y,y_pred,train,test,metric='mse'):
    if(metric=='mae'):
        m=y.mean()
        a=np.round(100*(np.abs(y_pred[train]-y[train])/(m)).mean(),4)
        b=np.round(100*(np.abs(y_pred[test]-y[test])/(m)).mean(),4)
    else:
        a=np.round(np.sqrt(mean_squared_error(y_pred[train],y[train])),4)
        b=np.round(np.sqrt(mean_squared_error(y_pred[test],y[test])),4)
    return(a,b)

# Differentiate Evaluation function.
def res_detail(Y,pred,train,test,flag_list,dim_g,metric='mse'):
    flag_list=flag_list.copy()
    anti_flag=np.zeros(len(train))
    for flag in flag_list:
        anti_flag+=flag
    flag_list.insert(0,(anti_flag==0))
    
    res_list=[]
    for flag in flag_list:
        res_list.append(print_res_aux(Y.reshape(-1,dim_g),pred.reshape(-1,dim_g),train&flag,test&flag,metric))
    res_list.append(print_res_aux(Y.reshape(-1,dim_g),pred.reshape(-1,dim_g),train,test,metric))
    res=np.concatenate(res_list,axis=0).reshape(-1)
    return(np.round(res,2))

#Weight update for Unsupervised weight loss anomaly penalisation
def update_w(Y,pred,var,train_rf,per=99.5,w_l_max=0.2,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    W=np.ones((dim_n,dim_t,1))
    
    s,_=sa.var_give(Y,np.zeros((dim_n*dim_t,dim_g)),var,train_rf,per,1,pmin=0,pmax=100)
    v=sa.Maha(s,train_rf,per)
    
    v[v>1]=1
    v[v<np.percentile(v,80)]=np.percentile(v,80)
    v = (v-np.percentile(v,80))/(1-np.percentile(v,80))
    W= W - v 
    W[W<w_l_max]=w_l_max
    return(W,1-(W+0.1)/2)