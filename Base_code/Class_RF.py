import time
import numpy as np
import pandas as pd
import pickle
import math
import multiprocess as mp
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor 
from sklearn.datasets import make_classification
from sklearn.model_selection import TimeSeriesSplit
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from sklearn.base import BaseEstimator
from sklearn.model_selection import train_test_split

class RF:
    #Usefull code link to Random forest regressor.
    
    def __init__(self):
        self.RF=RandomForestRegressor()
    
    #Fit function    
    def fit(self,X,Y,W=None):
        self.RF.fit(X,Y,W)
        
    #Predict function
    def predict(self,X):
        return(self.RF.predict(X))
    
    def greed_search_rf(self,X,y,train,test,dev_test=0,param_set=0):
        clf=RandomForestRegressor()
        folds = 3
        if(param_set==-1):
            params = {
                    'n_estimators':[50,100],
                    'max_depth': [5,7,10],
                    'min_samples_leaf': [1,2,5],
                    }

        elif(param_set==0):
            params = {
                    'n_estimators':[150,250],
                    'min_impurity_decrease':[0.2,0.4],
                    'max_features': [0.8,0.95],
                    'max_depth': [7,11,15],
                    'min_samples_split': [10,20],
                    'min_samples_leaf': [5,10],
                    }

        else:
            params = {
                    'n_estimators':[100,200,250,300,350],
                    'min_impurity_decrease':[0.05,0.1,0.2,0.4],
                    'max_features': [0.7,0.8,0.9],
                    'max_depth': [5,7,9,11,15],
                    'min_samples_split': [2,5,10,20],
                    'min_samples_leaf': [2,5,10],
                    }

        if(dev_test==1):
             params = {'n_estimators':[10,20]}

        tscv = TimeSeriesSplit(n_splits=folds)
        gr_search = GridSearchCV(clf,param_grid=params, 
                                 scoring='neg_mean_squared_error', 
                                 n_jobs=8,
                                 cv=tscv.split(X[train]))
        gr_search.fit(X[train], y[train])
        self.RF=gr_search.best_estimator_

    
    #Random search tunning
    def random_search_rf(self,X,y,train,test,n_esti=100,dev_test=0,param_set=1):
        clf=RandomForestRegressor()
        folds = 4
        param_comb = n_esti
        if(param_set==0):
            params = {
                    'n_estimators':[150,250,350],
                    'min_impurity_decrease':[0.1,0.2,0.3],
                    'max_features': [0.8,0.95],
                    'max_depth': [7,10,13,16],
                    'min_samples_split': [5,10,15],
                    'min_samples_leaf': [5,10,15],
                    }
        else:
            params = {
                    'n_estimators':[100,200,250,300,350],
                    'min_impurity_decrease':[0.05,0.1,0.2,0.4],
                    'max_features': [0.7,0.8,0.9],
                    'max_depth': [5,7,9,11,15],
                    'min_samples_split': [2,5,10,20],
                    'min_samples_leaf': [2,5,10],
                    }


        if(dev_test==1):
            params = {'n_estimators':[10,20]}

        tscv = TimeSeriesSplit(n_splits=folds)
        random_search = RandomizedSearchCV(clf,param_distributions=params, n_iter=param_comb, scoring='neg_mean_squared_error', n_jobs=8, cv=tscv.split(X[train]), verbose=1, random_state=1001)
        random_search.fit(X[train], y[train])
        self.RF=random_search.best_estimator_
        return()


    # Variance estmation by RF extraction
    # Auxilliar function based on Builded scikit Random forest regressor..
    def esti_var_rf_aux(self,c_e):
        import numpy as np
        e=c_e[0]
        n_f=e.shape[1]
        e_train=c_e[1]
        y=c_e[2]
        dim_g=y.shape[-1]
        train_rf=c_e[3]
        var_rf=np.zeros((len(e),dim_g))
        pred_rf=np.zeros((len(e),dim_g))
        #For each element
        for n,j in enumerate(e):
            #Recovers of the Weigh vector of learning sample for n ith element prediction.
            W_v=np.zeros(len(e_train))    
            #For each tree
            for k,i in enumerate(j):
                #Recover elements that share a leaf with n
                v=(e_train[:,k]==i)
                #Weight calculation
                W_v+=(v/v.sum())

            #Boolean flag of learning samples that discriminate active contribution.
            flag_W_v=(W_v>0)

            #Weighted averate = prédiction
            pred_rf[n]=(1/n_f)*((y[train_rf][flag_W_v])*np.tile(W_v[flag_W_v],(dim_g,1)).T).sum(axis=0)
            #Variance calucation
            var_rf[n]=np.sqrt((1/n_f)*(np.power((y[train_rf][flag_W_v]-pred_rf[n]),2)*np.tile(W_v[flag_W_v],(dim_g,1)).T).sum(axis=0))
        return(pred_rf,var_rf)

    #Variance estimation by RF extraction / Distributed computation
    def esti_var_rf(self,x,Y_xgb,train_rf,tab):
        pool=mp.Pool(6)
        e=self.RF.apply(x)
        e_train=e[train_rf]
        res=pool.map(self.esti_var_rf_aux,[(e[tab[i]:tab[i+1]],e_train,Y_xgb,train_rf) for i in range(len(tab)-1)])
        RF_mean=np.concatenate([i[0] for i in res])
        RF_var=np.concatenate([i[1] for i in res])
        pool.close()
        return(RF_var)