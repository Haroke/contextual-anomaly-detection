import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import Base_code.score_anomalie as sa
from sklearn.metrics import mean_squared_error,roc_curve


#Construction of score and peripheric using provided residu, variance and biais.
# r = residu, s= score local,v = score global, ps : thersohld percentiles (to recalculate boundary for visuation purpose)  

def compute_score(Y,list_r,Biais,Var,name_score,train_rf,per=98,dim=(400,20,3),type_norm='Percentiles_local'):
    n_score=len(list_r)
    list_sign= [sa.sign(r,dim) for r in list_r]
    list_s=[]
    list_v=[]
    list_ps=[]
    list_born=[]
    #For each scores = Triplet (Residu,biais,Var) performe score construction.
    for n in range(n_score): 
        print(name_score[n])
        
        #Compute score
        s,ps,v,pv=sa.var_give(list_r[n],Biais[n],Var[n],train_rf,per_norm=per,per_seuil=99.5,pmin_cut=0.1,pmax_cut=99.9,
                              d=1,type_norm=type_norm,filt=[0,1,0],q_var=1,dim=dim)
        #Stock normality bondary
        list_born.append(sa.borne(Y,list_r[n],Biais[n],Var[n],train_rf,ps=ps,q_var=1,dim=dim))
        list_s.append(s)
        list_ps.append(ps)
        list_v.append(v)
    return(list_s,list_v,list_ps,list_sign,list_born,name_score)

# Calucaltion of detection result from booléan anomaly vector (anom). 
def compute_res(list_s,list_v,anom,impact_anom,name_score,roc_all,res_matrix_,norm_v,n_exp,dim):
    dim_n,dim_t,dim_g=dim
    flag_period=np.arange(dim_n*dim_t)%dim_t//6
    #For each kind of local score 
    for n,s in enumerate(list_s): 
        
        #For each spatial dimension
        for g in range(dim_g):
           
            l=[]
            
            #Number of generated anoamly detected.
            n_l=(((np.abs(s[:,g])>=1) & (anom>0)).sum())
            
            #Evaluation of detection number by context and by anoamly magnitude.
            for i in range(4):
                for j in range(2):
                    l.append(((np.abs(s[:,g])>=1) & (flag_period==i)&(anom>0) & (anom%2==(j))).sum())
            
            #Result
            res_matrix_[n_exp,n,:,g]=np.concatenate([[n_l],l])

    #For each kind of global score
    for n,s in enumerate(list_v): 
        s=s.reshape(-1)

        l=[]
        #Number of generated anoamly detected
        n_l=(((s>=1) & (anom>0)).sum())
        
        #Evaluation of detection number by context and by anoamly magnitude.
        for i in range(4):
            for j in range(2):
                l.append(((s>=1) & (flag_period==i)&(anom>0) & (anom%2==(j))).sum())
        #print(g,name_score[n],n_l,(np.abs(impact_anom)>1).sum(),(s>1).sum(),l)
        
        #Result
        res_matrix_[n_exp,n,:,3]=np.concatenate([[n_l],l])

    #Number of anomaly by context and magnitude
    count_anom=[(anom>0).sum()]
    for i in range(4):
        for j in range(2):
            count_anom.append(((flag_period==i)&(anom>0) & (anom%2==j)).sum())
    norm_v[n_exp]=count_anom
    
    #Calculation of ROC metrics for local detection.
    for n,s in enumerate(list_s):
        for d in range(dim_g):
            roc=roc_curve(anom>1,np.abs(s).reshape(-1,3)[:,d],drop_intermediate=False)[1]
            while(len(roc)<(dim_n*dim_t+1)):
                roc=np.concatenate([roc,[1]])
            roc_all[n_exp,n,:,d]=roc
    
    #Calculation of ROC metrics for global detection.
    for n,v in enumerate(list_v):
        roc=roc_curve(anom>1,v.reshape(-1),drop_intermediate=False)[1]
        while(len(roc)<(dim_n*dim_t+1)):
            roc=np.concatenate([roc,[1]])
        roc_all[n_exp,n,:,3]=roc
    return(res_matrix_,roc_all)


# Display of detailled results
def mat_show(name_score,res_matrix_,norm_v,f_res=None):
    name_score=np.array(name_score)
    if(f_res==None):
        f_res=np.arange(len(name_score))<1000
    num_row=len(name_score[f_res])
    num_col=9

    fig=plt.figure(figsize=(12,10))
    
    plt.suptitle('Synthetic result',y=1.1)
    dim_g=res_matrix_.shape[-1]    
    g_title=['Local detection','Global detection']
    plt.subplots_adjust(hspace=0,wspace=0)
    f_flag=[np.arange(dim_g)<dim_g-1,np.arange(dim_g)==dim_g-1]

    
    for g,f in enumerate(f_flag):
        plt.subplots_adjust(wspace=0.)
        plt.subplot(1,2,g+1)
        if(g==0):
            plt.yticks(np.arange(len(name_score[f_res])),name_score[f_res][::-1],fontsize=9)
        else:
            plt.yticks(np.arange(len(name_score[f_res])),[])
        
        mat_reduc=(res_matrix_[:,:,:,f].mean(axis=-1).sum(axis=0)/norm_v[:].sum(axis=0))
        im=plt.imshow(mat_reduc,cmap=plt.get_cmap('Reds'),vmin=0,vmax=1,extent=(-0.5, num_col-0.5, -0.5, num_row-0.5))
        plt.title(g_title[g])
        plt.xticks(np.arange(9),['anom_tot','Anom_high_c1','Anom_low_c1','Anom_high_c2','Anom_low_c2',
                                 'Anom_high_c3','Anom_low_c3','Anom_high_c4','Anom_low_c4'],rotation=90)
        for (n_j,n_i),label in np.ndenumerate(mat_reduc):
            label=np.round(100*label,0).astype(int)
            if(label==0):
                label='X'
            plt.text(n_i,(num_row-1)-n_j,label,ha='center',va='center',fontsize=12)
        
        cbar=fig.colorbar(im, orientation='vertical', shrink=0.3,ticks=[0, 1])
        cbar.ax.set_yticklabels(['0','100'])
        if(g==3):
            cbar.set_label('%detection', rotation=270,labelpad=12)
           
    plt.tight_layout()
    plt.show()
    return()

# Display of rocs curves
def roc_show(roc_all,name_score,n_exp,color):
    plt.figure(figsize=(16,6))
    dim_g=roc_all.shape[-1]    
    str_dim_g=['Local','Global']
    plt.subplots_adjust(hspace=0,wspace=0)
    f_flag=[np.arange(dim_g)<dim_g-1,np.arange(dim_g)==dim_g-1]
    for g,f in enumerate(f_flag):
        plt.subplot(1,2,g+1)
        plt.title('ROC '+str_dim_g[g])
        for i in range(len(name_score)):
            roc= roc_all[:,:,:,f].mean(axis=-1).mean(axis=0)[i,:]
            plt.plot((np.arange(len(roc_all[0,0]))/len(roc_all[0,0])),roc,
                     label=name_score[i],color=color[i],alpha=0.95)
            plt.ylabel('Sensibility')
            plt.xlabel('1- Specificity')
        plt.legend(fontsize=9)
        plt.xlim(0,0.15)
        plt.ylim(0.5,1.01)
    plt.tight_layout()
    plt.savefig('roc')
    return()

# Display of synthesized results
def show_mat_comp(res_matrix_,Type_modele,Type_variance,pos,n_anom,per):
    dim_g=res_matrix_.shape[-1]    
    Type_modele=np.array(Type_modele)
    Type_variance=np.array(Type_variance)
    n_model=len(Type_variance)
    comp_res = np.zeros((len(Type_modele),len(Type_variance),dim_g))
    for k in range(dim_g):
        for n,i in enumerate(res_matrix_.sum(axis=0)[:,0,k]):
            comp_res[pos[n]//n_model,pos[n]%n_model,k]=i

    active_model=comp_res[:,:,0].sum(axis=1)>0
    active_score=comp_res[:,:,0].sum(axis=0)>0    
    
    Type_modele=np.array(Type_modele)[active_model]
    Type_variance=np.array(Type_variance)[active_score]
    
    fig=plt.figure(figsize=(16,4))
    str_dim_g=[]

    str_dim_g=['Local','Global']
    plt.subplots_adjust()
    f_flag=[np.arange(dim_g)<dim_g-1,np.arange(dim_g)==dim_g-1]
    mat_pro=[]
    for i,f in enumerate(f_flag):
        plt.subplot(1,2,i+1)
        plt.title('Detection performance '+str_dim_g[i]+'for threesold fix to 2% ')
        num_row=len(Type_modele)
        num_col=len(Type_variance)
        mat_reduc=comp_res[active_model][:,active_score][:,:,f].mean(axis=2)/(n_anom)
        mat_pro.append(mat_reduc)
        im=plt.imshow(mat_reduc,cmap=plt.get_cmap('Reds'),vmin=0,vmax=1,extent=(-0.5, num_col-0.5, -0.5, num_row-0.5))
        plt.yticks(np.arange(len(Type_modele)),np.array(Type_modele)[::-1])
        plt.xticks(np.arange(len(Type_variance)),Type_variance,rotation=45)
        for (n_j,n_i),label in np.ndenumerate(mat_reduc):
            label=np.round(100*label,0).astype(int)
            if(label==0):
                label = 'X'
            plt.text(n_i,(num_row-1)-n_j,label,ha='center',va='center',fontsize=12)
        cbar=fig.colorbar(im, orientation='vertical', shrink=0.5,ticks=[0, 1])
        cbar.ax.set_yticklabels(['0','100'])
    plt.tight_layout()
    plt.savefig('res_compress')
    plt.show()
    return(mat_pro,active_model,active_score)