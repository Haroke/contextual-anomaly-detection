import numpy as np
import pandas as pd
import math
import time
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

#data_generation 

#Periodical pattern
def base_cos_freq(array,freq):
    l=[]
    for i in freq:
        l.append(np.cos(i*math.pi*array))
        l.append(np.sin(i*math.pi*array))
    return(np.concatenate(l).reshape(len(freq)*2,-1).T)

#Motifs generation
def gen_motif(verbose=0,s_min=10,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    dim_n,dim_t,dim_g=dim
    n_motif=int(dim_g*2)
    L_pattern_day=[]
    Day_Motif=np.ones(dim_t)
    for n,i in enumerate([0.25,0.5,0.70,0.90]):
        Day_Motif[n]=i
        Day_Motif[-n]=i
        
    s_min = 200+500*s_min+np.zeros(dim_g)
    s1=np.random.randint(-7,7,dim_g*2)
    s2=np.random.randint(-7,7,dim_g*2)
    d1=np.random.normal(1.5,0.5,dim_g*2)
    d2=np.random.normal(1.5,0.5,dim_g*2)
    c1=np.maximum(np.random.normal(1,0.5,dim_g*2),0.05)*(dim_t/20)
    c2=np.maximum(np.random.normal(1,0.5,dim_g*2),0.05)*(dim_t/20)

    for i in range(n_motif):         
     
        if(i<n_motif/2): 
            a=np.histogram(np.concatenate([np.random.normal(10,3,1000),
                                           np.random.normal(10+s1[i],d1[i],int(c1[i]*1000)),
                                           np.random.normal(10+s2[i],d2[i],int(c2[i]*1000))])
                           ,bins=dim_t,range=(0,20))
            L_pattern_day.append(Day_Motif*savgol_filter((s_min[i]+a[0])/(s_min[i]+a[0]).sum(), 9, 4))
        else:
            a=np.histogram(np.concatenate([np.random.normal(10,8,int(100)),
                                       np.random.normal(10+s1[i],d1[i],int(c1[i]*1000)),
                                       np.random.normal(10+s2[i],d2[i],int(c2[i]*1000))])
                           ,bins=dim_t,range=(0,20))
            L_pattern_day.append(savgol_filter(dim_t*np.random.normal(1,0.06)*((a[0]+10)/(a[0]+10).sum()), 9, 4))

    if(verbose):
        plt.figure(figsize=(11,4))
        for i in range(n_motif):
            plt.subplot(2,n_motif/2,i+1)
            plt.plot(L_pattern_day[i],label='dim1')
        plt.show()
    return(L_pattern_day)

#Seasonals partern generation
def gen_contexte(verbose=0,seas1=140,seas2=2000,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    seas=(np.sin(2*math.pi*np.arange(dim_t*dim_n)/seas2))/6 # 6 : Seas2 magnitude ponderation
    seas1=(-np.cos(2*math.pi*np.arange(dim_t*dim_n)/seas1))/3 # 3 : Seas1 magnitude ponderation
    if(verbose):
        plt.figure(figsize=(10,3))
        plt.title('Harmonique de contexte')
        plt.plot(1+seas,color='black')
        plt.plot(1+seas+seas1)
        plt.show()
    return(1+seas+seas1)

#Anomaly pattern genaration : 
#Experiment have been essentially realised with point anomaly.
#Detection based on non-point anomalies required specific methodology not defined in these implementations.
def gen_anom_motif(anom_point=True,verbose=False,dim=(400,20,3)):
        
    if(anom_point):
        p2=np.array([1,0,0,0,0])
        motif1=np.stack([p2,p2,p2])
        motif=[motif1,-motif1]
    #Non-point anomaly case.
    else:
        #Poisson law motifs.
        #p1=np.histogram(np.random.gamma(np.random.normal(11,2,1), scale=np.random.normal(0.8,0,1), size=10000),bins=5)[0]/10000
        #p2=np.histogram(np.random.gamma(np.random.normal(11,2,1), scale=np.random.normal(0.8,0,1), size=10000),bins=5)[0]/10000

        #Mannual motifs.
        p0=np.array([0,0,0,0,0])
        p1=np.array([1.4,1.1,0.7,0.4,0.3])
        p2=np.array([0.5,1.1,-0.6,-0.7,-0.3])
        p3=np.array([0.3,1,2.0,1,0.3])
        motif1=np.stack([p1,p1,p1])
        motif2=np.stack([p2,p2,-p3])
        motif3=np.stack([p0,p3,p3])
       
        motif=[motif1,-motif1,motif2,motif3]
    
    txt_anom=['Anom positive faible','Anom negative forte','Anom negative faible','Anom negative forte']
    if(verbose):
        fig=plt.figure(figsize=(6,4))
        for n,i in enumerate(motif[0:4]):
            plt.subplot(2,2,n+1)
            plt.title(txt_anom[n])
            im=plt.imshow(i*(1+(n//2)*0.4),vmin=-2,vmax=2,cmap='seismic')
            fig.colorbar(im, orientation='vertical', shrink=1)
            plt.xticks(np.arange(5),np.arange(5)-1)
            plt.show()
    return(motif)

#Anomaly generation
def gen_anom(n_anom,motif,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    n_motif=len(motif)
    len_motif=motif[0].shape[1]
    anom=np.zeros((dim_n*dim_t))
    impact_anom=np.zeros((dim_t*dim_n,3))
    anom_list=[]
    for n,i in enumerate(np.random.choice(np.arange(dim_n*dim_t-len_motif),n_anom,False)):       
        type_=np.random.randint(1,n_motif+1)
        anom[i]=type_*(1+(n%2))
        anom_list.append((i//dim_t,i%dim_t,type_,n%2))
        impact_anom[i:(i+len_motif),:]+=(motif[type_-1]*(1+(n%2)*0.5)).T
    return(anom_list,anom,impact_anom)

#Noise generation
def gen_noise(sigma,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    noise = np.random.normal(0,sigma,(dim_t*dim_n,3))
    return(noise)

#Generation of dynamic pattern through an arbitrary autoregressive pattern + randomly daily magnitude.
def gen_data_dyn(Motif_pattern,Motifs_contexte,var_jour,c_dyn,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    
    #ramdomly daily magnitude
    Ampleur_day = np.repeat(np.random.normal(1,var_jour,(dim_n,3)),dim_t,axis=0)
    data_brut= Motif_pattern * Motifs_contexte * Ampleur_day
    data_brut[data_brut<0]=0
    
    data_dyn=np.zeros((dim_t*dim_n,3))
          
    #Arbitrary autoregressive pattern.
    
    for i in range(0,dim_t*dim_n):
        data_dyn[i,0] = (2-(4*c_dyn))*data_brut[i,0]+2*c_dyn*data_brut[i-1,0]+c_dyn*data_brut[i-1,1]+c_dyn*data_brut[i-1,2]
        data_dyn[i,1] = (2-(4*c_dyn))*data_brut[i,1]+c_dyn*data_brut[i-1,0]+2*c_dyn*data_brut[i-1,1]+c_dyn*data_brut[i-1,2]
        data_dyn[i,2] = (2-(4*c_dyn))*data_brut[i,2]+c_dyn*data_brut[i-1,0]+c_dyn*data_brut[i-1,1]+2*c_dyn*data_brut[i-1,2]
    
    data_dyn = data_dyn
    
    return(data_dyn)

#Core Generation function.
def core_gen(c_dyn = 0.2,  # Magnitude of dynamic pattern.
             var_jour= 0.15, # Magnitude of random daily.
             n_anom = 120, #Anomaly number
             sigma_mult = 0.075, # mutliplicatife noise magnitude
             sigma_add =0.01, # additive noise magnitude
             f_anom=1, # General anomaly magnitude
             anom_seuil=0, # Minimal flat impact of anomaly
             verbose=0, # Display type
             anom_point=True, # Anomaly type
             seas1=140,  #first seasonal periodicity
             seas2=2000, #second seasonal periodicity 
             dim=(400,20,3)): #Dimension

    dim_n,dim_t,dim_g=dim
    per_anom = (1 - ((int(dim_n*dim_t*0.05)/4) / (dim_t*dim_n)))*100
    var_mult= np.abs(np.percentile(np.random.normal(0,sigma_mult,10000),per_anom))
    var_add = np.abs(np.percentile(np.random.normal(0,sigma_add,10000),per_anom))
    print(var_mult,var_add)

    #Generation of mean and variance basic pattern.
    L_pattern_day=gen_motif(verbose,s_min=(var_add)*(1+var_mult),dim=dim)
    
    #Generation of seasonal influence
    contexte=gen_contexte(verbose,seas1=seas1,seas2=seas2,dim=dim)
    seasonalities = np.concatenate([(contexte)[:,None],(contexte)[:,None],(contexte)[:,None]],axis=1)
    
    #Concatenation of the mean pattern of each spatial dimension  
    Mean_pattern = np.concatenate([np.tile((L_pattern_day[0]),dim_n)[:,None],np.tile((L_pattern_day[1]),dim_n)[:,None],np.tile((L_pattern_day[2]),dim_n)[:,None]],axis=1)
    
    #Concatenation of the variance pattern of each spatial dimension
    Variance_pattern = np.concatenate([np.tile((L_pattern_day[3]),dim_n)[:,None],np.tile((L_pattern_day[4]),dim_n)[:,None],np.tile((L_pattern_day[5]),dim_n)[:,None]],axis=1)
    Variance_contexte = seasonalities * Variance_pattern

    #Generation of dynamic pattern over the crossing of mean motifs with seasonal influences.
    data_dyn=gen_data_dyn(Mean_pattern,seasonalities,var_jour,c_dyn,dim)
    
    #Generation of noises
    data_bruit_mult = gen_noise(sigma_mult,dim)
    data_bruit_add = gen_noise(sigma_add,dim)

    #Injection of variance (Combination of Variance pattern and mutliplicatif noise) and additif noise.
    data_dyn=data_dyn/(4*data_dyn.mean())
    data = (data_dyn * (1 + data_bruit_mult * Variance_contexte) + data_bruit_add)
    
    #Generation of anomaly
    motif=gen_anom_motif(anom_point,verbose=0,dim=dim)
    anom_list,anom,impact_anom=gen_anom(n_anom,motif,dim)

    #Injection of anomaly.
    #Anomaly magnitude taking into account the variance and the noise to be dicernable.
    data_full = data *  ( 1 + (2*var_mult * Variance_contexte) * f_anom*impact_anom) + ((2*var_add)+anom_seuil)* f_anom*impact_anom

    env_up = 1000*((data_dyn*(1+Variance_contexte*var_mult))+var_add)
    env_mid = 1000*data_dyn
    env_inf = 1000*((data_dyn*(1-Variance_contexte*var_mult))-var_add)

    var_true = (env_up - env_inf)/2

    return(1000*data,1000*data_full,impact_anom,anom,anom_list,env_up,env_mid,env_inf,var_true,Variance_contexte)

#Long term feature construction.
def factory(data_full,seas1=140,seas2=2000,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    Y=data_full.reshape(dim_n*dim_t,dim_g)

    # Construction des features long termes.
    X_time_h=base_cos_freq((np.arange(dim_t*dim_n)%dim_t)/dim_t,[2])
    X_seas1_h=base_cos_freq((np.arange(dim_t*dim_n)%seas1)/seas1,[2])
    X_seas2_h=base_cos_freq((np.arange(dim_t*dim_n)%seas2)/seas2,[2])

    X_lt=np.array(np.concatenate([X_time_h,X_seas1_h,X_seas2_h],axis=1))
    
    train_rf=np.arange(dim_t*dim_n)<(dim_t*dim_n*2/3)
    test_rf=np.arange(dim_t*dim_n)>=(dim_t*dim_n*2/3)

    train=np.arange(dim_n)<(dim_n*2/3)
    test=np.arange(dim_n)>=(dim_n*2/3)
    return(Y,X_lt,train,test,train_rf,test_rf)

#Short-term feature construction.
def factory_st(X_lt,Y,dim_f=4,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    l=[]
    for i in range(1,dim_f+1):
        l.append(np.roll(Y,i,axis=0))
    Y_past=np.array(l).swapaxes(0,1).reshape(dim_n*dim_t,dim_f*dim_g)
    X_st=np.concatenate([X_lt,Y_past],axis=1)
    return(X_st)

#Display of data curve with mean and variance.
def plot_var(Y,impact_anom,anom,env_mid,Variance_contexte,flag_day,sigma_mult,sigma_add,d=9,dim=(400,20,3)):
    dim_n,dim_t,dim_g=dim
    anom_pred=((np.abs(impact_anom).sum(axis=-1)>0).astype(int) -  (anom>0).astype(int))
    if(anom_pred.sum()<1):
        anom_pred[0]=1
        anom_pred[-1]=1

    fig, ax = plt.subplots(dim_g*2,1,figsize=(13,10),gridspec_kw={'height_ratios': [1,3]*dim_g})
    for step in range(dim_g):
        norm=Y.mean(axis=0)

        anom_pred=((np.abs(impact_anom).sum(axis=-1)>0).astype(int) -  (anom>0).astype(int))
        if(anom_pred.sum()<1):
            anom_pred[0]=1
            anom_pred[-1]=1

        f_obs=(np.arange(dim_n*dim_t)> (0*dim_t)) & (np.arange(dim_n*dim_t) <= ((d)*dim_t))
        xlim=max(np.max(Y[f_obs,0]*1.1),np.max(env_mid[f_obs,0]*1.2))

        ax[step*2].set_title('Exemple '+str(step+1)+' of a randomly generated series',fontsize=13)
        ax[step*2+1].set_title('Series '+str(step+1)+' with variance illustration',fontsize=13)
        ax[step*2].plot(Y[f_obs,step],label='Noised series',color='black')
        ax[step*2].set_ylim(min(np.percentile(Y[f_obs,step],0.5)*1.3,-50),np.percentile(Y[f_obs,step],99.5)*1.3)
        ax[step*2].set_xticks(np.arange(9)*dim_t)

        data_dyn=env_mid/1000

        ni =[2,8,15,25,50]
        color_full = [(0.5,0.0,0.5),(0.8,0,0),(0.8,0.6,0),(0,0.8,0),(0,0.4,0),(0,0.8,0),(0.8,0.6,0),(0.8,0,0),(0.5,0.0,0.5)]
        color_full2 = [(0.5,0.0,0.5),(0.8,0,0),(0.8,0.6,0),(0,0.8,0),(0,0.4,0),(0,0.4,0),(0,0.8,0),(0.8,0.6,0),(0.8,0,0),(0.5,0.0,0.5)]

        per=[]
        for i in  [0,1,5,10,25,75,90,95,99,100]:
            var_mult= np.percentile(np.random.normal(0,sigma_mult,10000),i)
            var_add = np.percentile(np.random.normal(0,sigma_add,10000),i)
            signal=1000*((data_dyn*(1+Variance_contexte*var_mult))+var_add)
            per.append(signal)
            
        env_up=per[-3]

        for i in range(len(per)-1):
            ax[step*2+1].fill_between(np.arange(f_obs.sum()), per[i][f_obs,step], per[i+1][f_obs,step],color=color_full[i],alpha=0.20)
        for i in range(len(per)):
            ax[step*2+1].plot(np.arange(f_obs.sum()), per[i][f_obs,step],color=color_full2[i],linewidth=0.5,alpha=0.40)
            if(i>4):
                ax[step*2+1].fill_between([], [], [],color=color_full2[i],label=str(ni[9-i])+'% Coverage',alpha=0.20)

        ax[step*2+1].plot(Y[f_obs,step],label='Noised series',color='black')
        if(i>1):
            f_anom=np.repeat(np.abs(impact_anom)[f_obs,step]>0,7)
            for i in range(8):
                f_anom+=np.roll(f_anom,i-4)
            f_anom=f_anom>0
            ax[step*2+1].fill_between(np.arange(f_obs.sum()*7)/7-(3/7), -1000,Y[f_obs,step].max()*1.2, 
                                      where=f_anom,facecolor='blue', alpha=0.2,label='Anomaly',zorder=-10)
        ax[step*2+1].set_ylim(min(per[1][f_obs,step].min()*1.2,-50),per[-2][f_obs,step].max()*1.2)
        ax[step*2+1].set_xticks(np.arange(d)*dim_t)
        ax[step*2+1].legend(ncol=7) 
        ax[step*2+1].set_xlim(0,(d)*dim_t)
    plt.tight_layout()
    plt.show()