import numpy as np
import tensorflow as tf
import keras
import keras.backend as K
from keras import metrics,optimizers,regularizers
from keras.layers import Activation,Concatenate,Multiply,Layer,Input,Reshape,RepeatVector,TimeDistributed,Bidirectional,Lambda, add, multiply, Dropout, Embedding, Flatten, Dense
from keras.models import Model
from keras.losses import mse
path='../../../../../scratch/kevin.pasini/save/'

class Lstm:
    #LSTM E-D forecast on a sequence ensemble (day) of element (time step) multidimensional (station).
    #Model learning on daily sequence object by reccurent process.
    
    def __init__(self,
             dim_n=400, # Sequence number (DAY)
             dim_t=20,  # Sequence size . (Hour)
             dim_lt=33, # Long term features dimension.
             dim_g=3,   # Spatial dimension
             dim_f=5,   # Past horizon depth
             fact_dim=[150,75], #Factory layer sizes
             gen_dim=[150,75], #Generatatoire layer sizes
             dim_z=100, # Reccurent layers sizes.
             dp=0.08, #Dense layer dropout.
             dp_2=0.12, # Reccurent layer dropout
             coef_mse=1000, #Lweight of loss
             testing=False, #Testing mode.
             name='0'): #Name
        
        
        self.dim_n=dim_n
        self.dim_t=dim_t 
        self.dim_lt=dim_lt 
        self.dim_g=dim_g 
        self.dim_f=dim_f
        self.fact_dim=fact_dim
        self.gen_dim=gen_dim
        self.dim_z=dim_z
        self.dp=dp
        self.dp_2=dp_2
        self.coef_mse=coef_mse
        self.testing=testing
        self.name=name
        
        #Contextual features (dim_t,dim_lt,dim_n) -> (dim_t,dim_lt,batch size).
        i0=Input(shape=(dim_t,dim_lt,),name='LT')
        #Dynamic features -> (dim_t,dim_f,dim_g,dim_n) -> (dim_t,dim_f,dim_g, batch size)
        i1=Input(shape=(dim_t,dim_f,dim_g),name='ST') 
        c_Y_past=Lambda(lambda x:K.reshape(x,(-1,dim_t,dim_f*dim_g)))(i1)

        #Main layer definition.
        #Factory : Contextual preprocessing layers.
        fact_layers=[]
        for n,i in enumerate(fact_dim):
            fact_layers.append(Dense(i,activation='sigmoid',name='fact_'+str(n)))

        #Génération : Traduction/Genération layers.
        gen_layers=[]
        for n,i in enumerate(gen_dim):
            gen_layers.append(Dense(i,activation='sigmoid',name='gen_'+str(n)))
        gen_layers.append(Dense(dim_g,activation='linear',name='gen_'+str(len(gen_dim))))

        #Encoder LSTM
        lstm_enc0=keras.layers.LSTM(int(dim_z),
                                  name='LSTM_enc_0',
                                  return_sequences=True,
                                  activation='sigmoid',
                                  recurrent_dropout=dp,
                                  kernel_regularizer=keras.regularizers.l1_l2(l1=0.0001, l2=0.0001))
        #Encoder decoder LSTM
        lstm_dec1=keras.layers.LSTM(dim_z,
                              name='LSTM_dec_1',
                              return_sequences=True,
                              stateful=False,
                              return_state=False,
                              activation='sigmoid',
                              recurrent_dropout=dp,
                              kernel_regularizer=keras.regularizers.l1_l2(l1=0.0001, l2=0.0001))

        #layer, Connexion.        
        
        #Factory 
        h_fact = i0
        for n,i in enumerate(fact_layers):
            if(n==0):
                h_fact = TimeDistributed(i)(h_fact)
            else:
                h_fact = TimeDistributed(i)(Dropout(dp)(h_fact))

        lstm_in = Lambda(lambda x: K.concatenate(x)[:,:,:],name='Concat_H0_Z0')([h_fact,c_Y_past])

        #Encoding
        c_h1=Dropout(dp)(lstm_enc0(lstm_in))

        #Shape modidification
        lstm_dec1_in = Lambda(lambda x:  K.reshape(K.repeat_elements(x[:,:,None,:],dim_f,2),(-1,dim_f,dim_z)),name='repeat_h1')(c_h1)

        #Decoding
        h_gen = lstm_dec1(lstm_dec1_in)

        #Generation = Decoded Embedding transcription. 
        for n,i in enumerate(gen_layers):
            if(n==len(gen_layers)-1):
                h_gen=TimeDistributed(i)(h_gen)
            else:
                h_gen=TimeDistributed(i)(Dropout(dp)(h_gen))

        gen_out=Lambda(lambda x:K.reshape(x,(-1,dim_t,dim_f,dim_g)),name='Pred_ct')(h_gen)
        # M : FULL model
        self.M = Model([i0,i1],gen_out)
        # Mcut Partial model : Encoded embedding recovering.
        self.Mcut = Model([i0,i1],c_h1)

    def build_loss(self,pond_):
        #Ponderation loss.
        pond_= np.array(pond_).astype(np.float32)
        def Loss(Y_true,Y_pred):
            dim_batch=K.shape(Y_true)[0]
            return(K.square(Y_true-Y_pred)*K.tile(pond_[None,None,:,None],[dim_batch,self.dim_t,1,self.dim_g])*self.coef_mse*self.dim_f)
        return(Loss)
    
    def modify_dropout(self,dp,dp_2):
        self.M.save_weights('test')
        self.__init__(dim_n=self.dim_n,
                      dim_t=self.dim_t,
                      dim_lt=self.dim_lt,
                      dim_g=self.dim_g,
                      dim_f=self.dim_f,
                      fact_dim=self.fact_dim,
                      gen_dim=self.gen_dim,
                      dim_z=self.dim_z,
                      dp=dp,
                      dp_2=dp_2,
                      coef_mse=self.coef_mse,
                      testing=self.testing,
                      name=self.name)
        self.M.load_weights('test')
        
    def get_param(self):
        #Parmeter recover
        dict_param={}
        dict_param['dim_n']=self.dim_n
        dict_param['dim_t']=self.dim_t
        dict_param['dim_lt']=self.dim_lt
        dict_param['dim_g']=self.dim_g
        dict_param['dim_f']=self.dim_f
        dict_param['fact_dim']=self.fact_dim
        dict_param['gen_dim']=self.gen_dim
        dict_param['dim_z']=self.dim_z
        dict_param['dp']=self.dp
        dict_param['dp_2']=self.dp_2
        dict_param['coef_mse']=self.coef_mse
        dict_param['testing']=self.testing  
        dict_param['name']=self.name
        return(dict_param)


    def fit(self,Input,Output,train,test,epochs=[1000,1000],b_s=[100,20],l_r=[0.01,0.005],sample_w=None,n_repet=1,pond_=None,verbose=0):
        #Training function
        call0=keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=5, patience=40, verbose=0, mode='min',restore_best_weights=True)
        call1=keras.callbacks.ReduceLROnPlateau(monitor='loss', min_delta=5, factor=0.3, patience=20, verbose=0, mode='min', cooldown=0, min_lr=0.00005)

        Input_train=[i[train] for i in Input]
        Input_test=[i[test] for i in Input]
        Output_train=[o[train] for o in Output]
        Output_test=[o[test] for o in Output]

        if(len(Input)==1):
            Input_train=Input_train[0]
            Input_test=Input_test[0]

        if(len(Output)==1):
            Output_train=Output_train[0]
            Output_test=Output_test[0]
        
        if(pond_==None):
            pond_=np.zeros((n_repet,dim_f))
            pond_[0,0]=1
            pond_[1:]=1
 
        for n in range(n_repet):
            for i in range(len(b_s)):
                self.M.compile(optimizer=optimizers.nadam(lr=l_r[i]),loss=self.build_loss(pond_[0]))
                self.M.fit(Input_train,Output_train,
                       validation_data=(Input_train,Output_train),
                       epochs=epochs[i],
                       batch_size=b_s[i],
                       sample_weight=sample_w,
                       callbacks=[call0,call1],verbose=verbose)

            for i in range(len(b_s)):
                self.M.compile(optimizer=optimizers.nadam(lr=l_r[i]),loss=self.build_loss(pond_[n+1]))
                self.M.fit(Input_train,Output_train,
                       validation_data=(Input_train,Output_train),
                       epochs=epochs[i],
                       batch_size=b_s[i],
                       sample_weight=sample_w,
                       callbacks=[call0,call1],verbose=verbose)

    def load(self,name=None):
        #Weight  load
        if(name):
            self.M.load_weights(path+name)
        else:
            self.M.load_weights(path+'Lstm_ed_'+self.name)
        
    def save(self,name=None):
        #Weight save
        if(name):
            self.M.save_weights(path+name)
        else:
            self.M.save_weights(path+'Lstm_ed_'+self.name)
                        
    def predict(self,V1,Y_past,norm):
        #T+1 classic prediction
        pred=np.roll((self.M.predict([V1,Y_past])[:,:,1]*norm).reshape(-1,self.dim_g),1,axis=0)
        return(pred)

    def predict_variationel(self,V1,Y_past,norm,n_ech=50):
        #Variational prediction + variance estimation for step T+1 et T+4(lag)
        fun_pred = K.function(self.M.inputs + [K.learning_phase()], self.M.outputs)
        dim_g=self.dim_g
        dim_f=self.dim_f
        pred=[]
        for i in range(n_ech):
            print(i)
            pred.append(fun_pred([V1,Y_past] + [1])[0]*norm)

        var_lstm=np.roll(np.array(pred).std(axis=0)[:,:,1].reshape(-1,dim_g),1,axis=0)
        preds=np.array(pred).mean(axis=0).reshape(-1,dim_f,dim_g)
        pred_lstm=np.roll(preds[:,1],1,axis=0)
        
        var_lstm_lag=np.roll(np.array(pred).std(axis=0)[:,:,-1].reshape(-1,dim_g),dim_f-1,axis=0)
        pred_lstm_lag=np.roll(preds[:,-1],dim_f-1,axis=0)
       
        return([pred_lstm,pred_lstm_lag],[var_lstm,var_lstm_lag],preds)
    
    